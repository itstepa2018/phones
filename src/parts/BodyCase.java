package parts;

public enum BodyCase {
    METAL("Silver"), PLASTIC("Black"), WOOD("Brown");
    private String color;


    BodyCase(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
