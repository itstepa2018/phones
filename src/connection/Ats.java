package connection;

import phones.AbstractPhone;
import utility.ConsoleInput;
import utility.Input;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public final class Ats {
    private Input consoleInput = new ConsoleInput();
    protected final Map<String, AbstractPhone> connectionNumbers = new HashMap<>();
    private int maxPhones;
    private static Ats instance;

    private Ats() {
    }

    public static Ats getInstance() {
        if (instance == null){
            instance = new Ats();
        }
        return instance;
    }
    public void connect(AbstractPhone phone) {
        if (connectionNumbers.containsKey(phone.getNumber())) {
            String input = consoleInput.getString("This number exists. Overwrite? Y/N");
            if (!input.equalsIgnoreCase("y")) {
                return;
            }
        }
        connectionNumbers.put(phone.getNumber(), phone);
    }

    public void disconnect(AbstractPhone phone) {
        if (connectionNumbers.remove(phone.getNumber()) != null) {
            System.out.println("phone disconnected.");
        } else {
            System.out.println(phone + " not connected.");
        }
    }

    public boolean link(String numberFrom, String numberTo) {
        if (!connectionNumbers.containsKey(numberTo)){
            return false;
        }
        AbstractPhone from = connectionNumbers.get(numberFrom);
        AbstractPhone to = connectionNumbers.get(numberTo);
        from.outgoingCall(numberTo);
        to.incomingCall();
        return true;
    }
}
