package connection;

public interface Connector {
    String send();

    void receive(String message);
}
