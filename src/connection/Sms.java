package connection;

import java.util.List;

public class Sms {
    private List <String> numbers;
    private String text;

    public List<String> getNumbers() {
        return numbers;
    }

    public void setNumbers(List<String> numbers) {
        this.numbers = numbers;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "Sms{" +
                "numbers=" + numbers +
                ", text='" + text + '\'' +
                '}';
    }
}
