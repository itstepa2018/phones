package connection;

import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class Connection {
    private Connector caller;
    private Connector callee;
    private List<String> messages = new LinkedList<>();

    public void connect(Connector callee, Connector caller){

        boolean who = true;
        while (true){
            String message;
            if (who){
                message = callee.send();
                caller.receive(message);
            }else {
                message = caller.send();
                callee.receive(message);
            }
            who=!who;
            if (message.equalsIgnoreCase("#0end")){
                System.out.println("Connection finished");
                break;
            }

        }
    }

}
