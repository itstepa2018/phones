package main;

import connection.Ats;
import parts.CaseColor;
import phones.AbstractPhone;
import phones.ConsolePhoneFactory;
import phones.models.smartphones.GooglePixel2Xl;
import phones.models.smartphones.SamsungS9;
import phones.models.smartphones.XiaomiRedmi6A;
import phones.models.wired.GrandMaPhone;
import utility.AbstractPhoneFactory;
import utility.ConsoleInput;
import utility.FixedListPhoneFactory;

import java.util.LinkedList;
import java.util.List;

public class Main {
    private List<AbstractPhone> phones = new LinkedList<>();
    private AbstractPhoneFactory factory = new ConsolePhoneFactory();
    private ConsoleInput consoleInput = new ConsoleInput();

    private void start() {
        boolean isContinue = true;
        while (isContinue) {
            System.out.println("1 - Actions with the phone");
            System.out.println("3 - Add new phone");
            System.out.println("4 - Phone book");
            System.out.println("5 - connect phone");
            System.out.println("6 - disconnect phone");
            System.out.println("0 - Exit");
            String input = consoleInput.getString();
            switch (input) {
                case "1":
                    phoneActions();
                    break;
                case "2":
                    System.out.println("Under construction");
                    break;
                case "3":
                    createPhone();
                    break;
                case "4":
                    for (AbstractPhone abstractPhone : phones) {
                        System.out.println(abstractPhone);
                    }
                    break;
                case "5":
                    connectPhone();
                    break;
                case "6":
                    disconnectPhone();
                    break;
                case "0":
                    isContinue = false;
                    break;
                default:
                    System.out.println("Try again");
            }
            consoleInput.getScanner().hasNextLine();
        }
    }

    private void disconnectPhone() {
        Ats.getInstance().disconnect(findPhoneByNumber(consoleInput.getString("Input phone number")));
    }

    private void connectPhone() {
        AbstractPhone phone = findPhoneByNumber(consoleInput.getString("Input phone number"));
        if (phone != null) {
            Ats.getInstance().connect(phone);
            System.out.println("phone connected");
        } else {
            System.out.println("phone not exists");
        }
    }

    private AbstractPhone findPhoneByNumber(String number) {
        for (AbstractPhone phone : phones) {
            if (phone.getNumber().equalsIgnoreCase(number)) {
                return phone;
            }
        }
        return null;
    }

    private void createPhone() {
        System.out.println("1 - Pixel");
        System.out.println("2 - GrandMaPhone");
        String[] list = FixedListPhoneFactory.getPhoneList();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < list.length; i++) {
            System.out.println((i + 1) + " - " + list);
            stringBuilder.append(i + 1);
            stringBuilder.append(" - ");
            stringBuilder.append(list[i]);
            stringBuilder.append("\n");
        }
        System.out.println(stringBuilder.toString());
        String input = consoleInput.getInstance().getString();
        Integer number;
        try {
            Integer number = Integer.parseInt(input);
        } catch (NumberFormatException exception) {
            System.out.println("Incorrect input");
        }
        if (number >= list.length | number <= 0) {
            System.out.println("Incorrect number");
        }
        try {

        }
    }


    private void phoneActions() {
        AbstractPhone phone = findPhoneByNumber(consoleInput.getString("Input phone number: "));
        if (phone == null) {
            System.out.println("Phone not exists");
            return;
        }
        System.out.println("1 - Send SMS");
        System.out.println("2 - Call");
        System.out.println("3 - Assign a number");
        System.out.println("Back - Any other key");
        switch (consoleInput.getString()) {
            case "1":
                System.out.println("Under construction");
                break;
            case "2":
                String targetNumber = consoleInput.getString("Input target number");
                boolean connectionResult = Ats.getInstance().link(phone.getNumber(), targetNumber);
                if (!connectionResult) {
                    System.out.println("Connection failed");
                }
                break;
            case "3":
                System.out.println("Under construction");
                break;
            default:
        }
    }

//    public static void main(String[] args) {
//        Main main = new Main();
//        main.start();
//        System.out.println("bye");
//    }

    public static void main1(String[] args){
        SamsungS9 samsungS9 = new SamsungS9(4,null);
        System.out.println(samsungS9.getBodyCase().getColor());
    }

    public static void main(String[] args) {
        AbstractPhone phone;
        if (Math.random() > 0.5){
            phone= new SamsungS9();
        }else {
            phone = new XiaomiRedmi6A();
        }
        if (phone instanceof SamsungS9){
            System.out.println("Yeaa");
        }
        CaseColor caseColor = CaseColor.BLACK;
        System.out.println(caseColor);
    }
}
