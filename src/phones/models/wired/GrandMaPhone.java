package phones.models.wired;

import phones.AnaloguePhone;

public class GrandMaPhone extends AnaloguePhone {

    public static final int DefaultWeight = 10;

    public GrandMaPhone() {
        super(DefaultWeight);
    }

    @Override
    public void incomingCall() {
        super.incomingCall();
    }

    @Override
    public void ring() {
        System.out.println("ph-ph-ph-ph");
    }
}
