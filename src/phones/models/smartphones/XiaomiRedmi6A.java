package phones.models.smartphones;

import phones.InternetAccess;
import phones.NotificationSound;
import phones.PolyphonySound;
import phones.SmartPhone;

public class XiaomiRedmi6A extends SmartPhone {
    public XiaomiRedmi6A() {
        super(.4, new InternetAccess() {
            @Override
            public void sendData() {
                System.out.println("Sending data");
            }

            @Override
            public void receiveData() {
                System.out.println("Receiving data");
            }
        });
    }
    @Override
    protected PolyphonySound createPolyphonySound() {
        return new PolyphonySound() {
            @Override
            public void play() {
                System.out.println("XiaomiRingMelody.mp3");
            }
        };
    }

    @Override
    protected NotificationSound createNotificationSound() {
        return new NotificationSound() {
            @Override
            public void play() {
                System.out.println("XiaomiMelody.mp3");
            }
        };
    }

}
