package phones.models.smartphones;

import phones.InternetAccess;
import phones.NotificationSound;
import phones.PolyphonySound;
import phones.SmartPhone;

public class GooglePixel2Xl extends SmartPhone {
    public GooglePixel2Xl() {
        super(0.34, new InternetAccess() {
            @Override
            public void sendData() {
                System.out.println("Sending data");
            }

            @Override
            public void receiveData() {
                System.out.println("Receiving data");
            }
        });
    }

    @Override
    protected PolyphonySound createPolyphonySound() {
        return new PolyphonySound() {
            @Override
            public void play() {
                System.out.println("Pixel2XlRingMelody.mp3");
            }
        };
    }

    @Override
    protected NotificationSound createNotificationSound() {
        return new NotificationSound() {
            @Override
            public void play() {
                System.out.println("Pixel2XlMelody.mp3");
            }
        };
    }
}
