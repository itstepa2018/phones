package phones.models.smartphones;

import phones.InternetAccess;
import phones.NotificationSound;
import phones.PolyphonySound;
import phones.SmartPhone;

public class SamsungS9 extends SmartPhone {

    public SamsungS9(double weight, InternetAccess internet) {
        super(weight, internet);
    }

    @Override
    protected PolyphonySound createPolyphonySound() {
        return new PolyphonySound() {
            @Override
            public void play() {
                System.out.println("");
            }
        };
    }

    @Override
    protected NotificationSound createNotificationSound() {
        return new NotificationSound() {
            @Override
            public void play() {
                System.out.println("biz-biz");
            }
        };
    }
}
