package phones;

public abstract class SmartPhone extends GsmPhone {
    private InternetAccess internet;

    public SmartPhone(double weight, InternetAccess internet) {
        super(weight);
        this.internet = internet;
    }

}
