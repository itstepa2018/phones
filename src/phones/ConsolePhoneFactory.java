package phones;

import phones.models.smartphones.GooglePixel2Xl;
import phones.models.smartphones.XiaomiRedmi6A;
import phones.models.wired.GrandMaPhone;
import utility.AbstractPhoneFactory;

public class ConsolePhoneFactory extends AbstractPhoneFactory {
    @Override
    public AbstractPhone createPhone(String phoneModel) {
        if (phoneModel.equalsIgnoreCase(GrandMaPhone.class.getSimpleName())) {
            return new GrandMaPhone();
        } else if (phoneModel.equalsIgnoreCase(GooglePixel2Xl.class.getSimpleName())) {
            return new GooglePixel2Xl();
        } else if (phoneModel.equalsIgnoreCase(XiaomiRedmi6A.class.getSimpleName())) {
            return new XiaomiRedmi6A();
        } else {
            throw new PhoneCreationException("Unknown phone model " + phoneModel + ", phone can't be created!");
        }
    }
}
