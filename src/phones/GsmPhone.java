package phones;

import connection.Sms;

public abstract class GsmPhone extends Wireless implements ISms {
    private PolyphonySound sound;
    private NotificationSound smsSound;

    public GsmPhone(double weight) {
        super(weight);
        this.sound = createPolyphonySound();
        this.smsSound = createNotificationSound();
    }

    protected abstract PolyphonySound createPolyphonySound();

    protected abstract NotificationSound createNotificationSound();

    @Override
    public void sendSms(Sms sms) {
        System.out.println("Sending SMS: " + sms);
    }

    @Override
    public void receiveSms(Sms sms) {
        smsSound.play();
        System.out.println("You have an SMS " + sms);
    }

    @Override
    public void ring() {
        sound.play();
    }
}
