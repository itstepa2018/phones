package phones;

import connection.Sms;

public interface ISms {
    void sendSms (Sms sms);

    void receiveSms (Sms sms);
}
