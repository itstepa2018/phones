package phones;

import phones.models.smartphones.GooglePixel2Xl;
import phones.models.smartphones.SamsungS9;
import phones.models.smartphones.XiaomiRedmi6A;
import phones.models.wired.GrandMaPhone;
import utility.ConsoleInput;
import utility.PhoneCreationException;

public class ConsoleInputPhoneFactory extends AbstractPhoneFactory {

    public ConsoleInputPhoneFactory () {
        this.input = ConsoleInput.getInstance();
    }
    @Override
    public AbstractPhone createPhone(String phoneType) throws PhoneCreationException {
        if (phoneType.equals(GooglePixel2Xl.class.getSimpleName())) {
            return new GooglePixel2Xl();
        } else if (phoneType.equals(GrandMaPhone.class.getSimpleName())) {
            return new GrandMaPhone();
        } else if (phoneType.equals(SamsungS9.class.getSimpleName())) {
            return new SamsungS9();
        } else if (phoneType.equals(XiaomiRedmi6A.class.getSimpleName())) {
            return new XiaomiRedmi6A();
        }
    }
}
