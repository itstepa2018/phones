package phones;

import utility.Input;

public abstract class AbstractPhoneFactory {
    Input input;

    public abstract AbstractPhone createPhone(String phoneType);
}
