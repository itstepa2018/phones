package phones;

import parts.BodyCase;
import parts.CaseColor;

import java.io.Serializable;
import java.util.Random;

public abstract class AbstractPhone implements Ringable, Callable, Serializable {
    private BodyCase BodyCase;
    private double weight;
    private String number;

    protected AbstractPhone(double weight) {
        super();
        this.weight = weight;
        this.BodyCase = BodyCase;
        this.BodyCase.setColor("Black");
    }

    public parts.BodyCase getBodyCase() {
        return BodyCase;
    }

    public void setBodyCase(parts.BodyCase bodyCase) {
        BodyCase = bodyCase;
    }

    @Override
    public void ring() {
        System.out.println("bzzz");
    }

    @Override
    public void incomingCall() {
        ring();
        System.out.println("We have incoming call on " + getNumber());
    }

    @Override
    public void outgoingCall(String number) {
        System.out.println("We are calling from " + getNumber() + " to " + number);
    }

    public void generateNumber() {
        this.number = String.valueOf(100 + new Random().nextInt(900));
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }



    @Override
    public String toString() {
        return getClass().getSimpleName() +
                " weight=" + weight +
                ", number='" + number + '\'' +
                '}';
    }
}
