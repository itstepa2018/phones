package phones;

public interface Callable {
    void incomingCall();

    void outgoingCall(String number);
}
