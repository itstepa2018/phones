package utility;

import phones.AbstractPhone;
import phones.ConsoleInputPhoneFactory;
import phones.models.smartphones.GooglePixel2Xl;
import phones.models.smartphones.SamsungS9;
import phones.models.smartphones.XiaomiRedmi6A;
import phones.models.wired.GrandMaPhone;

public class FixedListPhoneFactory extends ConsoleInputPhoneFactory {
    private String [] phoneList = {GooglePixel2Xl.class.getSimpleName(), XiaomiRedmi6A.class.getSimpleName(),
                                            SamsungS9.class.getSimpleName(), GrandMaPhone.class.getSimpleName()};

    @Override
    public  AbstractPhone createPhone(String phoneType) throws  PhoneCreationException {
        for (String type: phoneList){
            if (type.equals(phoneType)){
                return super.createPhone(phoneType);
            }
        }
        throw new PhoneCreationException("There are no such phone class: " + phoneType);
    }

}
