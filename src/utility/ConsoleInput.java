package utility;

import java.util.Scanner;

public class ConsoleInput implements Input {
    private Scanner scanner = new Scanner(System.in);

    public static Input getInstance() {
        return null;
    }

    @Override
    public String getString(String msg) {
        System.out.println(msg);
        return scanner.nextLine();
    }

    @Override
    public String getString() {
        return scanner.nextLine();
    }

    public Scanner getScanner() {
        return scanner;
    }
}
