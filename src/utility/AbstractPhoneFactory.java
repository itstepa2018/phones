package utility;

import phones.AbstractPhone;

public abstract class AbstractPhoneFactory {
    Input input;
    public abstract AbstractPhone createPhone(String phoneModel);
}
