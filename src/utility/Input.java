package utility;

public interface Input {
    String getString(String msg);
    String getString();
}
